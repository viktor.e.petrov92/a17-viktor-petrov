package com.example.progresstask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProgressTaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProgressTaskApplication.class, args);
    }

}
