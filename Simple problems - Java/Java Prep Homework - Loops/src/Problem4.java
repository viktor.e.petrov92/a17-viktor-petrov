import java.util.Scanner;

public class Problem4 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String card = scanner.nextLine();
        String currentCard = "2";
        int cardNumber = 2;
        switch (card) {
            case "J":
                cardNumber = 11;
                break;
            case "Q":
                cardNumber = 12;
                break;
            case "K":
                cardNumber = 13;
                break;
            case "A":
                cardNumber = 14;
                break;
            default:
                cardNumber = Integer.parseInt(card);
                break;
        }
        for (int i = 2; i <= cardNumber; i++) {
            switch (i) {
                case 11:
                    currentCard = "J";
                    break;
                case 12:
                    currentCard = "Q";
                    break;
                case 13:
                    currentCard = "K";
                    break;
                case 14:
                    currentCard = "A";
                    break;
                default:
                    currentCard = Integer.toString(i);
                    break;
            }
            System.out.printf("%s of spades, %s of clubs, %s of hearts, %s of diamonds",
                    currentCard, currentCard, currentCard, currentCard);
            System.out.println();
        }


    }
}