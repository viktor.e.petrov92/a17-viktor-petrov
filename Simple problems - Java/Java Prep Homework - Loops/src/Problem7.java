import java.math.BigInteger;
import java.util.Scanner;

public class Problem7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());
        int k = Integer.parseInt(scanner.nextLine());
        int token = n - k;
        BigInteger result;
        if (1 < n && 1 < k && n < 100 && k < 100) {
            result = factorial(n).divide(factorial(k).multiply(factorial(token)));
            System.out.println(result);
        }
    }
    public static BigInteger factorial(int num) {
        if (num <= 1)
            return BigInteger.ONE;
        else
            return factorial(num - 1).multiply(BigInteger.valueOf(num));
    }
}
