import java.text.DecimalFormat;
import java.util.Scanner;

public class Problem5 {

    public static void main(String[] args) {
        DecimalFormat df = new DecimalFormat("###0.00000");
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());
        double X = Double.parseDouble(scanner.nextLine());
        double factorial = 1;
        double multiX = 1;
        double result = 1;
        if (n >= 2 && n <= 10) {
            for (int i = 1; i <= n; i++) {
                factorial *= i;
                multiX *= X;
                result += factorial / multiX;
            }
            System.out.println(df.format(result));
        }
    }
}
