import java.text.DecimalFormat;
import java.util.Scanner;

public class Problem6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());
        int k = Integer.parseInt(scanner.nextLine());
        int nFactorial=1;
        int kFactorial=1;
        for (int i = 1; i <=n ; i++) {
            nFactorial*=i;
            if (i<=k){
                kFactorial*=i;
            }

        }
        int result = nFactorial/kFactorial;
        System.out.println(result);

    }
}
