
import java.util.Scanner;

public class Problem10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] input = scanner.nextLine().split(" ");
        int[] numbers = new int[input.length];
        int evenSum = 1;
        int oddSum = 1;
        for (int i = 0; i < input.length; i++) {
            numbers[i] = Integer.parseInt(input[i]);
        }
        for (int i = 1; i <= numbers.length; i++) {
            if (i % 2 == 0) {
                evenSum *= numbers[i - 1];
            }
            else oddSum *= numbers[i - 1];
        }
        if (evenSum==oddSum){
            System.out.print("yes");
        }else{
            System.out.print("no");
        }

    }

}

