import java.util.Scanner;

public class Problem13 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        int count = 0;
        int divider = 5;
        while ((number/divider) > 0) {
            count += number/divider;
            divider = divider * 5;
        }
        System.out.println(count);
    }
}
