import java.math.BigInteger;
import java.util.Scanner;

public class Problem8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());
        int a = n * 2;
        int b = n + 1;
        BigInteger result = null;
        if (0 <= n && n <= 100) {
            result = factorial(a).divide(factorial(b).multiply(factorial(n)));
        }
        System.out.println(result);
    }

    private static BigInteger factorial(int num) {
        if (num <= 1)
            return BigInteger.ONE;
        else
            return factorial(num - 1).multiply(BigInteger.valueOf(num));
    }
}

