import java.util.Scanner;

public class Problem11 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // if given integers are entered on a single line":
        int n = scanner.nextInt();
        int min = scanner.nextInt();
        int max = scanner.nextInt();

        //In case the integers are given in different lines:
        //int n = Integer.parseInt(scanner.nextLine());
        //int min = Integer.parseInt(scanner.nextLine());
        //int max = Integer.parseInt(scanner.nextLine());

        for (int i = 0; i < n; i++) {
            System.out.print((int) (Math.random() * ((max - min) + 1)) + min+" ");
        }
    }
}
