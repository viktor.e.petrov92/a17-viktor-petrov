import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());
        String biggestKeg ="";
        double biggestVolume = 0;
        while (n>0){
            String model = scanner.nextLine();
            double radius = Double.parseDouble(scanner.nextLine());
            int height = Integer.parseInt(scanner.nextLine());
             double volume = Math.PI* Math.pow(radius,2)*height;
             if (volume>biggestVolume){
                 biggestVolume=volume;
                 biggestKeg=model;
             }
             n--;

        }
        System.out.println(biggestKeg);
    }
}
