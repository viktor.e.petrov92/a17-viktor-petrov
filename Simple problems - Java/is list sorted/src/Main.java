import java.util.*;
import java.lang.Object;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int i;
        String[] input = scanner.nextLine().split(",");
        List<String> oldList = new ArrayList<>(input.length);
        List<String> newList = new ArrayList<>(oldList);
        for (i = 0; i < input.length; i++) {
            oldList.add(input[i]);
            for (String element : oldList
            ) {
                if (!newList.contains(element)) {
                    newList.add(element);
                }
            }
        }System.out.println(String.join(",", newList));


    }
}
