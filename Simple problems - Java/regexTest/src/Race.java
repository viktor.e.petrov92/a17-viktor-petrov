import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Race {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] participants = scanner.nextLine().split(", ");
        Map<String, Integer> racers = new LinkedHashMap<>();
        for (int i = 0; i < participants.length; i++) {
            racers.putIfAbsent(participants[i], 0);
        }
        String regexName = "[A-Za-z]";
        String regexDigit = "\\d";
        Pattern namePattern = Pattern.compile(regexName);
        Pattern digitPatter = Pattern.compile(regexDigit);

        String token = "";
        while (!"end of race".equals(token = scanner.nextLine())) {
            Matcher nameMatch = namePattern.matcher(token);
            StringBuilder name = new StringBuilder();
            while (nameMatch.find()) {
                name.append(nameMatch.group());
            }

            int distance = 0;
            if (racers.containsKey(name.toString())) {
                Matcher digitMatch = digitPatter.matcher(token);
                while (digitMatch.find()) {
                    distance += Integer.parseInt(digitMatch.group());
                }
                racers.put(name.toString(), racers.get(name.toString()) + distance);
            }

        }
        int[] count = {1};
        racers.entrySet()
                .stream()
                .sorted((a, b) -> b.getValue() - a.getValue())
                .limit(3)
                .forEach(e -> {
                    switch (count[0]) {
                        case 1:
                            System.out.println("1st place: " + e.getKey());
                            break;
                        case 2:
                            System.out.println("2nd place: " + e.getKey());
                            break;
                        case 3:
                            System.out.println("3rd place: " + e.getKey());
                    }
                    count[0]++;

                });
    }
}
