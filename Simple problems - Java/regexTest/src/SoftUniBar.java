import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SoftUniBar {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input="";
        String regex = "%(?<name>[A-Z][a-z]+)%[^|$%.]*<(?<product>\\w+)>[^|$%.]*\\|[^|$%.]*(?<quantity>\\d+)[^|$%.]*\\|[^|$%.\\d+]*(?<price>\\d+\\.?\\d+)\\$";
        Pattern pattern = Pattern.compile(regex);
        double totalIncome = 0;
        while (!"end of shift".equals(input=scanner.nextLine())){
            Matcher matcher = pattern.matcher(input);
            if (matcher.find()){
            String name = matcher.group("name");
            String product = matcher.group("product");
            int quantity =Integer.parseInt( matcher.group("quantity"));
            double price = Double.parseDouble(matcher.group("price"));
            double totalPrice = quantity*price;
            totalIncome+=totalPrice;
                System.out.printf("%s: %s - %.2f", name,product,totalPrice);
                System.out.println();
            }

        }
        System.out.printf("Total income: %.2f" ,totalIncome);
    }
}
