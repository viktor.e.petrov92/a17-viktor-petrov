import java.util.*;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        List<String> elements = Arrays.stream(input.split("\\s+"))
                .collect(Collectors.toList());
        String line;
        while (!"End".equals(line = scanner.nextLine())) {
            String[] tokens = line.split("\\s+");
            String command = tokens[0];
            switch (command) {
                case "Add":
                    String number = tokens[1];
                    elements.add(number);
                    break;
                case "Insert":
                    number = tokens[1];
                    int index = Integer.parseInt(tokens[2]);
                    if (0 <= index && index <= elements.size()) {
                        elements.add(index, number);
                    } else {
                        System.out.println("Invalid index");
                    }

                    break;
                case "Remove":
                    index = Integer.parseInt(tokens[1]);
                    if (0 <= index && index <= elements.size()) {
                        elements.remove(index);
                    } else {
                        System.out.println("Invalid index");
                    }
                    break;
                case "Shift":
                    if (tokens[1].equals("left")) {
                        int count = Integer.parseInt(tokens[2]);
                        Collections.rotate(elements, -count);
                    } else if (tokens[1].equals("right")) {
                        int count = Integer.parseInt(tokens[2]);
                        Collections.rotate(elements, count);
                    }
                    break;
            }
        }
        System.out.println(String.join(" ", elements));
    }
}
