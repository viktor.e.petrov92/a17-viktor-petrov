import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());
        int[] people = new int[n];
        int sum = 0;
        for (int i = 0; i <n; i++) {
            people[i]= Integer.parseInt(scanner.nextLine());
            sum+=people[i];
        }
        System.out.println(Arrays.toString(people)
                .replace("[","")
                        .replace("]","")
                        .replace(",",""));

        System.out.println(sum);
    }
}
