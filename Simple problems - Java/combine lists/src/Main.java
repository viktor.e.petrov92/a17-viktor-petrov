import java.util.Scanner;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Main {
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();
        String[] arr1 = line.split(",");
        String line2 = scanner.nextLine();
        String[] arr2 = line2.split(",");
        String[] newString = new String[arr1.length + arr2.length];
        for (int i = 0, j = 0; i < newString.length; i = i + 2, j++) {
            newString[i] = arr1[j];
        }
        for (int k = 1, l = 0; k < newString.length; k = k + 2, l++) {
            newString[k] = arr2[l];
        }
        System.out.println(String.join(",", newString));

    }


}