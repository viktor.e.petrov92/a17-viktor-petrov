import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] firstLine = Arrays.stream(scanner.nextLine().split("\\s"))
                .mapToInt(Integer::parseInt).toArray();
        int[] secondLine = Arrays.stream(scanner.nextLine().split("\\s"))
                .mapToInt(Integer::parseInt).toArray();
        int N = firstLine[0];
        int M = firstLine[1];
        int J = firstLine[2];

        int[][] matrix = new int[N][M];
        int temp = 1;
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++) {
                matrix[i][j] = temp++;
            }
        }
        int startRow = secondLine[0];
        int startCol = secondLine[1];
        long sum = matrix[startRow][startCol];
        boolean[][] visited = new boolean[N][M];
        visited[startRow][startCol] = true;
        int[] targetRows = new int[J];
        int[] targetCols = new int[J];
        for (int i = 0; i < J; i++) {
            String[] input = scanner.nextLine().split("\\s");
            targetRows[i] = Integer.parseInt(input[0]);
            targetCols[i] = Integer.parseInt(input[1]);
        }
        int curr = 0;
        while (true) {
            int targetR = targetRows[curr] + startRow;
            int targetC = targetCols[curr] + startCol;
            if (targetR >= N || targetR < 0 || targetC >= M || targetC < 0) {
                System.out.printf("escaped %d", sum);
                break;
            }
            if (visited[targetR][targetC]) {
                System.out.printf("caught %d", sum);
                break;
            }
            startRow = targetR;
            startCol = targetC;
            sum += matrix[startRow][startCol];
            visited[startRow][startCol] = true;
            curr++;
            curr%=J;
        }

    }
}
