import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        boolean[] isPrime = new boolean[n + 1];
        for (int i = 0; i <= n; i++) {
            boolean isIPrime = true;
            for (int j = 2; j <= i-1; j++) {
                if (i % j == 0) {
                    isIPrime = false;
                }
            }
            if (isIPrime) {
                isPrime[i] = true;
            } else {
                isPrime[i] = false;
            }
        }
        for (int i = 1; i <= n; i++) {
            if (isPrime[i]) {
                for (int j = 1; j <= i; j++) {
                    if (isPrime[j]) {
                        System.out.printf("1");
                    } else {
                        System.out.printf("0");
                    }

                }System.out.println();
            }
        }

    }
}
