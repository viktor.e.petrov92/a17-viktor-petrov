import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        char labels = input.next().charAt(0);
        int ranks = input.nextInt();
        if (labels == 'a' || labels == 'c' || labels == 'e' || labels == 'g') {
            switch (ranks) {
                case 1:
                case 3:
                case 5:
                case 7:
                    System.out.println("dark");
                    break;
                case 2:
                case 4:
                case 6:
                case 8:
                    System.out.println("light");
                    break;
            }
        } else if (labels == 'b' || labels == 'd' || labels == 'f' || labels == 'h') {
            switch (ranks) {
                case 1:
                case 3:
                case 5:
                case 7:
                    System.out.println("light");
                    break;
                case 2:
                case 4:
                case 6:
                case 8:
                    System.out.println("dark");
                    break;
            }
        }
    }
}
