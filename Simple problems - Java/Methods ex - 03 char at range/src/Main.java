
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String start = scanner.nextLine();
        String end = scanner.nextLine();
        char a = start.charAt(0);
        char b = end.charAt(0);
        missingChars(a, b);

    }

    private static void missingChars(char a, char b) {
        int start = Math.min( a,b);
        int end = Math.max(a,b);
        for (int i =++start; i < (int)end; i++) {
            StringBuilder sb = new StringBuilder();
            sb.append((char)i);
            sb.append(' ');
            System.out.print(sb);
        }

    }
}
