import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = Integer.parseInt(scanner.nextLine());
        Integer[] numbers = new Integer[N];
        Integer[] digits = new Integer[N];
        Integer[] result1 = new Integer[1024];
        Integer[] result2 = new Integer[1024];
        for (int i = 0; i < N; i++) {
            numbers[i] = Integer.parseInt(scanner.nextLine());
        }
        int a = 0;
        int b = 0;
        while (a < (N - 1)) {
            int digit = numbers[a] % 10;
            int digit2 = (numbers[a + 1] / 10) % 10;
            result1[b] = digit * 10 + digit2;
            a++;
            b++;
        }
        for (int i = 0; i <= b - 2; i++) {
            System.out.print(result1[i] + " ");
        }
        System.out.print(result1[b - 1]);
        a = 0;
        b = 0;
        while (a < (N - 1)) {
            int digit1 = (numbers[a] / 10) % 10;
            int digit2 = numbers[a] % 10;
            int digit3 = (numbers[a + 1] / 10) % 10;
            int digit4 = numbers[a + 1] % 10;
            if (digit2+digit3>=10){
                result2[b]= digit1*100+((digit2+digit3)%10)*10+digit4;
            }
            else {
                result2[b]= digit1*100+(digit2+digit3)*10+digit4;
            }
            a++;
            b++;
        }
        System.out.println();
        for (int i = 0; i <=b-2 ; i++) {
            System.out.print(result2[i]+" ");
        }
        System.out.print(result2[b-1]);

    }
}
