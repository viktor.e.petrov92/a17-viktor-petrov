import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());
        IsTopNumber(n);
    }
    private static void IsTopNumber(int number)
    {
        for (int i = 1; i <= number; i++)
        {
            int sum = 0;
            boolean oddDigit = false;
            int copy = i;
            while (true)
            {
                if (copy == 0) {
                    break;
                }
                int right = copy % 10;
                sum += right;
                if (!(right % 2 == 0)) oddDigit = true;
                copy /= 10;
            }
            if(sum%8==0 && oddDigit==true)
            {
                System.out.println(i);
            }

        }
    }
}
