


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String word = scanner.nextLine();
        int N = Integer.parseInt(scanner.nextLine());
        String[] anagrams = new String[N];
        for (int i = 0; i < N; i++) {

                anagrams[i] = scanner.nextLine();

        }
        boolean status = true;
        for (String elem : anagrams
        ) {
            if (word.length() != (elem).length()) {
                status = false;
            } else {
                char[] w = word.toCharArray();
                char[] anagramsChars = (elem).toCharArray();
                Arrays.sort(w);
                Arrays.sort(anagramsChars);
                status = Arrays.equals(w, anagramsChars);

            }
            if (status) {
                System.out.println("Yes");
            } else {
                System.out.println("No");
            }
        }
    }
}

