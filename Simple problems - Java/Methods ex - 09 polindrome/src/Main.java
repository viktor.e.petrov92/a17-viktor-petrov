import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input ="";
        while (!(input = scanner.nextLine()).equals("END")){
            checkPalindrome(input);
        }
    }
    private static void checkPalindrome(String s)
    {
        String reverse = new StringBuffer(s).reverse().toString();
        if (s.equals(reverse))
            System.out.println("true");
        else
            System.out.println("false");
    }
}
