import java.util.Scanner;

public class Problem4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double a = Double.parseDouble(scanner.nextLine());
        double b = Double.parseDouble(scanner.nextLine());
        double c = Double.parseDouble(scanner.nextLine());
        if (a*b*c>0){
            System.out.println("+");
        }else if(a*b*c==0){
            System.out.println("0");
        }else {
            System.out.println("-");
        }
    }
}
