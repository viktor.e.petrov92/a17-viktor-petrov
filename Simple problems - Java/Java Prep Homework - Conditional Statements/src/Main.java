import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double a = Double.parseDouble(scanner.nextLine());
        double b = Double.parseDouble(scanner.nextLine());
        if (a < b) {
            if (a % 1 == 0 && b % 1 == 0) {
                System.out.println((int) a + " " + (int) b);
            } else {
                System.out.println(a + " " + b);
            }
        } else {
            if (a % 1 == 0 && b % 1 == 0) {
                System.out.println((int) b + " " + (int) a);
            } else {
                System.out.println(b + " " + a);
            }

        }
    }
}
