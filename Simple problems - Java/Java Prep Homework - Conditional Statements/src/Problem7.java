import java.text.DecimalFormat;
import java.util.Scanner;

public class Problem7 {
    public static void main(String[] args) {
        DecimalFormat df = new DecimalFormat("#####.###");
        Scanner scanner = new Scanner(System.in);
        double a = Double.parseDouble(scanner.nextLine());
        double b = Double.parseDouble(scanner.nextLine());
        double c = Double.parseDouble(scanner.nextLine());
        double max = 0;
        double mid = 0;
        double min = 0;
        if (a >= b && a >= c) {
            max = a;
            if (b >= c) {
                mid = b;
                min = c;
            } else {
                mid = c;
                min = b;
            }
        }
        if (b >= a && b >= c) {
            max = b;
            if (a >= c) {
                mid = a;
                min = c;
            } else {
                mid = c;
                min = a;
            }
        }
        if (c >= a && c >= b) {
            max = c;
            if (a >= b) {
                mid = a;
                min = b;
            } else {
                mid = b;
                min = a;
            }
        }
        System.out.println(df.format(max) + " "
                + df.format(mid) + " " + df.format(min));


    }
}
