import java.util.Scanner;

public class Problem9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please choose a type:");
        System.out.println("1--> int");
        System.out.println("2--> double");
        System.out.println("3--> string");
        int type = Integer.parseInt(scanner.nextLine());
        switch (type) {
            case 1:
                System.out.println("Please enter a int:");
                int integer = Integer.parseInt(scanner.nextLine());
                System.out.println(integer + 1);
                break;
            case 2:
                System.out.println("Please enter a double:");
                double number = Double.parseDouble(scanner.nextLine());
                System.out.println(number + 1);
                break;
            case 3:
                System.out.println("Please enter a string:");
                String input = scanner.nextLine();
                System.out.println(input + "*");
        }
    }
}
