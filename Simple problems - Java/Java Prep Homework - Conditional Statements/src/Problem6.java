import java.util.Scanner;

public class Problem6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Double[] numbers = new Double[5];
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = Double.parseDouble(scanner.nextLine());
        }
        double max = numbers[0];
        for (Double elem : numbers
        ) {
            if (elem>max){
                max=elem;
            }
        }
        if (max%1==0){
            System.out.println((int)max);
        }else {
            System.out.println(max);
        }
    }
}
