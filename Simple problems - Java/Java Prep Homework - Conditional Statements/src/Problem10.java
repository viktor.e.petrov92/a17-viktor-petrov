import java.util.Scanner;

public class Problem10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        scanner.useDelimiter(":|\\s+");
        int h= scanner.nextInt();
        int m = scanner.nextInt();
        String tt= scanner.next();
        if (h>=1&&h<=12){
            if (m>=0&&m<=59){
                if (tt.equals("PM")){
                    System.out.println("beer time");
                }else if (tt.equals("AM")){
                    if (h<3) {
                        System.out.println("beer time");
                    }else {
                        System.out.println("non-beer time");
                    }
                }
            }
        }
    }
}
