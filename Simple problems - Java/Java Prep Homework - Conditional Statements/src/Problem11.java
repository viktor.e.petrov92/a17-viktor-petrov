import com.sun.xml.internal.ws.util.StringUtils;
import java.util.Scanner;

public class Problem11 {
    public static void main(String[] args) {
        int number = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter a number:");
        number = scanner.nextInt();
        if (number == 0) {
            System.out.print("Zero");
        } else if (number > 0 && number <= 999) {
            System.out.print(StringUtils.capitalize(numberToWord(number)));
        }
    }
    private static String numberToWord(int number) {
        String words = "";
        String[] unitsArray = {"zero", "one", "two", "three", "four", "five", "six",
                "seven", "eight", "nine", "ten", "eleven", "twelve",
                "thirteen", "fourteen", "fifteen", "sixteen", "seventeen",
                "eighteen", "nineteen"};
        String[] tensArray = {"zero", "ten", "twenty", "thirty", "forty", "fifty",
                "sixty", "seventy", "eighty", "ninety"};
        if (number == 0) {
            return "zero";
        }
        if ((number / 100) > 0) {
            words += numberToWord(number / 100) + " hundred and ";
            number %= 100;
        }
        if (number > 0) {
            if (number < 20) {
                words += unitsArray[number];
            } else {
                words += tensArray[number / 10];
                if ((number % 10) > 0) {
                    words += " " + unitsArray[number % 10];
                }
            }
        }
        return words;
    }
}
