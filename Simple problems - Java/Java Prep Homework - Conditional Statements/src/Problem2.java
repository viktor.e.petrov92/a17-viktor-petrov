import java.util.Scanner;

public class Problem2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());
        if (n>=1&&n<=9){
            if(n<=3){
                n=n*10;
                System.out.println(n);
            }else if (n <= 6){
                n=n*100;
                System.out.println(n);
            }else {
                n=n*1000;
                System.out.println(n);
            }
        }else{
            System.out.println("invalid score");
        }

    }
}
