import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        int result = vowelsCount(input);
        System.out.println(result);

    }

    private static int vowelsCount(String string) {
        int count = 0;
        for (int i = 0; i < string.length(); i++) {
            switch (string.toLowerCase().charAt(i)) {
                case 'a':
                case 'e':
                case 'i':
                case 'o':
                case 'u':
                case 'y':
                    count++;
            }
        }
        return count;


    }
}
