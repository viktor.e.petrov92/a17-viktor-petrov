
import java.util.Scanner;

public class CaesarCipher {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        char[] input = scanner.nextLine().toCharArray();

        for (int i = 0; i < input.length; i++) {
            input[i] += 3;
        }
        System.out.println(input);
    }
}
