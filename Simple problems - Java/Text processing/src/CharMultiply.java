import java.util.Scanner;

public class CharMultiply {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] input = scanner.nextLine().split(" ");
        System.out.println(MultiplyCharacters(input[0], input[1]));
    }

    public static long MultiplyCharacters(String str, String str2) {
        char[] first = str.toCharArray();
        char[] second = str2.toCharArray();

        long sum = 0;
        long tempSum = 0;
        int shorterWord = Math.min(first.length, second.length);
        int longestWord = Math.max(first.length, second.length);

        for (int i = 0; i < shorterWord; i++) {
            tempSum = (int) first[i] * (int) second[i];
            sum += tempSum;
        }

        if (first.length < second.length) {
            for (int i = shorterWord; i <= longestWord - 1; i++) {
                sum += second[i];
            }
        } else if (first.length > second.length) {
            for (int i = shorterWord; i <= longestWord - 1; i++) {
                sum += first[i];
            }
        }
        return sum;

    }

}
