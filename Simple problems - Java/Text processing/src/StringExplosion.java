import java.util.Scanner;

public class StringExplosion {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        char[] inputAsChars = input.toCharArray();
        int currPower = 0;
        StringBuilder result = new StringBuilder();
        boolean isPunch = false;
        for (int i = 0; i < inputAsChars.length; i++) {

            if (inputAsChars[i] == '>') {
                isPunch = true;
                continue;
            }
            if (isPunch) {
                currPower += inputAsChars[i] - 48;
                isPunch = false;
            }
            if (currPower > 0) {
                inputAsChars[i] = '0';
                currPower--;
            }
        }
        for (char ch : inputAsChars
        ) {
            if (ch != '0') {
                result.append(ch);
            }
        }
        System.out.println(result);
    }
}
