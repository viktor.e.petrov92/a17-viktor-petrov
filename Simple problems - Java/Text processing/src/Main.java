import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] input = scanner.nextLine().split(", ");
        Pattern pat = Pattern.compile("[:?!@#$%^&*()., ;'+=/|\\\\]+");

        for (int i = 0; i < input.length; i++) {
            if (input[i].length() >= 3 && input[i].length() <= 16) {
                Matcher match = pat.matcher(input[i]);
                if (!match.find())
                    System.out.println(input[i]);
            }
        }

    }
}
