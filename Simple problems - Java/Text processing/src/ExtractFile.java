import java.util.Scanner;

public class ExtractFile {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        String[] path = input.split("\\\\");
        String file = path[path.length - 1];
        String name = file.substring(0, file.lastIndexOf('.'));
        String extension = file.substring(file.lastIndexOf('.') + 1);
        System.out.println(String.format("File name: %s\nFile extension: %s",
                name, extension));
    }
}
