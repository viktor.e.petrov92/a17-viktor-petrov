import java.util.*;

public class CompanyUsers {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Map<String, ArrayList<String>> company = new TreeMap<>();
        String input = scanner.nextLine();
        while (!"End".equals(input)) {
            String[] tokens = input.split(" -> ");
            String companyName = tokens[0];
            String employee = tokens[1];
            if (!company.containsKey(companyName)) {
                company.put(companyName, new ArrayList<>());
                if (!company.get(companyName).contains(employee)) {
                    company.get(companyName).add(employee);
                }
            }
            else {
                if (!company.get(companyName).contains(employee)) {
                    company.get(companyName).add(employee);
                }
            }
            input = scanner.nextLine();
        }
        company.forEach((key, value) -> {
            System.out.println(key);
            for (String s : value) {
                System.out.println("-- " + s);
            }
        });
    }
}
