import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class AminerTask {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String input = sc.nextLine();
        Map<String, Long> resources = new LinkedHashMap<>();
        long quantity =0;
        while(!"stop".equals(input)){
            quantity = Integer.parseInt(sc.nextLine());
            if (!resources.containsKey(input)){
                resources.put(input,quantity);
            }else{
                long num = resources.get(input)+quantity;
                resources.put(input,num);
            }
            input=sc.nextLine();
        }
        for (Map.Entry<String, Long> entry : resources.entrySet()) {
            System.out.println(entry.getKey()+ " -> "+ entry.getValue());
        }

    }
}
