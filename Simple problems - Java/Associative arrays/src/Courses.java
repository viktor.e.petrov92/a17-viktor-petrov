import java.util.*;

public class Courses {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Map<String, ArrayList<String>> coursePerson = new LinkedHashMap<>();
        String line = scanner.nextLine();
        while (!line.equals("end")) {
            String[] tokens = line.split(" : ");
            String courseName = tokens[0];
            String userName = tokens[1];
            if (!coursePerson.containsKey(courseName)) {
                coursePerson.put(courseName, new ArrayList<>());
                coursePerson.get(courseName).add(userName);
            } else {
                coursePerson.get(courseName).add(userName);
                Collections.sort(coursePerson.get(courseName));
            }
            line = scanner.nextLine();
        }
        coursePerson.entrySet().stream().sorted((left, right) -> Integer.compare(right.getValue().size(), left.getValue().size()))
                .forEach(entry -> {
                    System.out.printf("%s: %d%n", entry.getKey(), entry.getValue().size());
                    for (int i = 0; i < entry.getValue().size(); i++) {
                        System.out.println("-- " + entry.getValue().get(i));
                    }
                });
    }
}
