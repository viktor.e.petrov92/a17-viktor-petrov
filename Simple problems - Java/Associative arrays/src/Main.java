
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();
        Map<Character, Integer> letters = new LinkedHashMap<>();
        char[] chars = line.toCharArray();
        for (char c : chars) {
            if (c != ' ') {
                letters.putIfAbsent(c, 0);
                letters.put(c, letters.get(c) + 1);
            }
        }
        for (Map.Entry<Character, Integer> entry : letters.entrySet()) {
            System.out.println(entry.getKey() + " -> " + entry.getValue());
        }

    }
}
