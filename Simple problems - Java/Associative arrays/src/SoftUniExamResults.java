import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class SoftUniExamResults {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Map<String, Integer> results = new HashMap<>();
        Map<String, Integer> submissions = new HashMap<>();
        String input = "";
        while (!"exam finished".equals(input = scanner.nextLine())) {
            String[] tokens = input.split("-");
            if (tokens.length > 2) {
                String name = tokens[0];
                String examName = tokens[1];
                int points = Integer.parseInt(tokens[2]);
                if (!results.containsKey(name)) {
                    results.put(name, points);
                } else if (results.get(name) < points) {
                    results.put(name, points);
                }

                if (!submissions.containsKey(examName)) {
                    submissions.put(examName, 1);
                } else {
                    submissions.put(examName, submissions.get(examName) + 1);
                }
            } else {
                String name = tokens[0];
                results.remove(name);
            }
        }
        System.out.println("Results:");
        results.entrySet().stream()
                .sorted(Map.Entry.<String, Integer>comparingByValue()
                        .reversed()
                        .thenComparing(Map.Entry.comparingByKey()))
                .forEach(entry -> {
                    System.out.println(String.format("%s | %d",
                            entry.getKey(), entry.getValue()));
                });

        System.out.println("Submissions:");
        submissions.entrySet().stream()
                .sorted(Map.Entry.<String, Integer>comparingByValue()
                        .reversed()
                        .thenComparing(Map.Entry.comparingByKey()))
                .forEach(entry -> {
                    System.out.println(String.format("%s - %d",
                            entry.getKey(), entry.getValue()));
                });
    }
}
