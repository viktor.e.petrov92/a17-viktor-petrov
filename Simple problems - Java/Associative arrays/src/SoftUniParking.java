
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class SoftUniParking {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Map<String, String> users = new LinkedHashMap<>();
        int n = Integer.parseInt(scanner.nextLine());
        for (int i = 0; i < n; i++) {
            String[] input = scanner.nextLine().split("\\s+");
            String command = input[0];
            switch (command) {
                case "register":
                    String userName = input[1];
                    String plateNumber = input[2];
                    if (users.containsKey(userName)) {
                        System.out.println("ERROR: already registered with plate number "
                                + users.get(userName));
                    } else {
                        users.put(userName, plateNumber);
                        System.out.printf("%s registered %s successfully", userName, plateNumber);
                        System.out.println();
                    }
                    break;
                case "unregister":
                    userName = input[1];
                    if (!users.containsKey(userName)) {
                        System.out.println("ERROR: user " + userName + " not found");
                    } else {
                        System.out.println(userName + " unregistered successfully");
                        users.remove(userName);
                    }
                    break;

            }
        }
        users.forEach((k, v) -> System.out.println(k + " => " + v));
    }
}
