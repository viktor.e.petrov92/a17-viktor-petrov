import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int year = scanner.nextInt();
        String[] zodiac = {"Monkey", "Rooster",
                "Dog", "Pig",
                "Rat", "Ox",
                "Tiger", "Hare", "Dragon", "Snake",
                "Horse", "Sheep",
        };
        System.out.println(zodiac[year % 12]);
    }
}
