import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
       String number = scanner.nextLine();
       int sum=0;
        for (char c :
                number.toCharArray()) {
            sum+=c - '0';

        }
        System.out.println(sum);
    }
}
