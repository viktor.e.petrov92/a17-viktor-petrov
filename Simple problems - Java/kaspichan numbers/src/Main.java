import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        List<String> characters = new ArrayList<>();
        for (char i = 'A'; i <= 'Z'; i++) {
            characters.add(Character.toString(i));
        }
        for (char i = 'a'; i <= 'i'; i++) {
            for (char j = 'A'; j <= 'Z'; j++) {
                characters.add(Character.toString(i) + j);
            }
        }
        Scanner scanner = new Scanner(System.in);
        long n = scanner.nextLong();
        String result = "";
        if (n==0){
            System.out.println("A");
        }
        while (n != 0) {
            result = characters.get((int)(n % 256)) + result;
            n = n / 256;
        }

        System.out.println(result);
    }
}
