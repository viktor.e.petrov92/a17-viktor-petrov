import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int i;
        String[] intToString = scanner.nextLine().split(", ");
        Integer[] numbers = new Integer[intToString.length];
        for (i = 0; i < intToString.length; i++) {
            numbers[i] = Integer.parseInt(intToString[i]);
        }
        Arrays.sort(numbers, Collections.reverseOrder());
        System.out.print(Arrays.toString(numbers).replace("[","").replace("]",""));


    }
}
