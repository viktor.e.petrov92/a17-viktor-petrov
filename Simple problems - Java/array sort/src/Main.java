import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();
        String[] strNumbers = line.split(",");
        List<String> result = new ArrayList<>();
        int zeros = 0;
        for (String elem:
             strNumbers) {
            if (elem.equals("0")){
                zeros++;
                continue;
            }
            result.add(elem);
        }
        for (int i = 0; i <zeros ; i++) {
            result.add("0");
        }
        System.out.println(String.join(",", result));
    }
}
