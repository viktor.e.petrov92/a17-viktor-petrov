import java.util.*;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] input = scanner.nextLine().split(" ");
        int pass = Integer.parseInt(input[0]);
        int K = Integer.parseInt(input[1]);
        int reversed = 0;
        while(pass != 0) {
            int digit = pass % 10;
            reversed = reversed * 10 + digit;
            pass /= 10;
        }
        int quotient = reversed / K;
        int remainder = reversed % K;
        int result = quotient+remainder;
        System.out.println(result);

    }
}
