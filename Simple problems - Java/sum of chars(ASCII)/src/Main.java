import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = Integer.parseInt(scanner.nextLine());
        int sum =0;

        for (int i = 1; i <=N ; i++) {
        String letter = scanner.nextLine();
        int ascii = letter.charAt(0);
        sum +=ascii;
        }
        System.out.println(sum);
    }
}
