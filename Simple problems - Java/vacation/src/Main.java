import java.text.DecimalFormat;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        DecimalFormat df = new DecimalFormat("#####.00");
        Scanner scanner = new Scanner(System.in);
        double people = Double.parseDouble(scanner.nextLine());
        String group = scanner.nextLine();
        String day = scanner.nextLine();
        double totalSum = 0.0;
        if (group.equals("Business") && people >= 100) {
            people = people - 10;
        }
        switch (group) {
            case "Students":
                switch (day) {
                    case "Friday":
                        totalSum = people * 8.45;
                        break;
                    case "Saturday":
                        totalSum = people * 9.80;
                        break;
                    case "Sunday":
                        totalSum = people * 10.46;
                        break;
                }
                break;
            case "Business":
                switch (day) {
                    case "Friday":
                        totalSum = people * 10.90;
                        break;
                    case "Saturday":
                        totalSum = people * 15.60;
                        break;
                    case "Sunday":
                        totalSum = people * 16.00;
                        break;
                }
                break;
            case "Regular":
                switch (day) {
                    case "Friday":
                        totalSum = people * 15.00;
                        break;
                    case "Saturday":
                        totalSum = people * 20.00;
                        break;
                    case "Sunday":
                        totalSum = people * 22.50;
                        break;
                }
                break;
        }
        if (group.equals("Students") && people >= 30) {
            totalSum = totalSum - (totalSum * 0.15);
        }
        if (group.equals("Regular") && people >= 10 && people <= 20) {
            totalSum = totalSum - (totalSum * 0.05);
        }
        System.out.println("Total price: "+df.format(totalSum));

    }
}