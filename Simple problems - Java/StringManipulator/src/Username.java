import java.util.Scanner;

public class Username {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String username = scanner.nextLine();
        String commands=scanner.nextLine();
        while (!"Sign up".equals(commands)){
            String[] tokens = commands.split(" ");
            String command = tokens[0];
            switch (command){
                case "Case":
                    String upperOrLower= tokens[1];
                    switch (upperOrLower){
                        case "upper":
                            username=username.toUpperCase();
                            System.out.println(username);
                            break;
                        case "lower":
                            username=username.toLowerCase();
                            System.out.println(username);break;
                    }
                    break;
                case "Reverse":
                    int startIndex= Integer.parseInt(tokens[1]);
                    int endIndex= Integer.parseInt(tokens[2]);
                    if (startIndex>=0&&startIndex<username.length()){
                        if (endIndex+1>startIndex&&endIndex+1<username.length()){
                            String token = username.substring(startIndex,endIndex+1);
                            StringBuilder reversed = new StringBuilder();
                            reversed.append(token);
                            reversed.reverse();
                            System.out.println(reversed);
                        }
                    }
                    break;
                case "Cut":
                    String subStr = tokens[1];
                    if (username.contains(subStr)){
                        username=username.replace(subStr,"");
                        System.out.println(username);
                    }else{
                        System.out.printf("The word %s doesn't contain %s.%n", username,subStr);
                    }
                    break;
                case "Replace":
                    String replace = tokens[1];
                    if (username.contains(replace)){
                       username= username.replace(replace,"*");
                        System.out.println(username);
                    }
                    break;
                case "Check":
                    String valid = tokens[1];
                    if (username.contains(valid)){
                        System.out.println("Valid");
                    }else{
                        System.out.println("Your username must contain "+ valid+".");
                    }
                    break;
            }

            commands=scanner.nextLine();
        }
    }
}
