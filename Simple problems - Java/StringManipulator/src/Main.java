import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String string=scanner.nextLine();
        String commands = scanner.nextLine();
        while (!commands.equals("Done")) {
            String[] tokens = commands.split("\\s+");
            String command = tokens[0];
            switch (command) {
                case "Change":
                    string = string.replace(tokens[1], tokens[2]);
                    System.out.println(string);
                    break;
                case "Includes":
                  if (string.contains(tokens[1])){
                      System.out.println("True");
                  }else{
                      System.out.println("False");
                  }
                    break;
                case "End":
                    if (string.endsWith(tokens[1])){
                        System.out.println("True");
                    }else{
                        System.out.println("False");
                    }
                    break;
                case "Uppercase":
                    string=string.toUpperCase();
                    System.out.println(string);
                    break;
                case "FindIndex":
                    System.out.println(string.indexOf(tokens[1]));
                    break;
                case "Cut":
                    int startIndex = Integer.parseInt(tokens[1]);
                    int length = Integer.parseInt(tokens[2]);
                    int endIndex = startIndex+length;
                    string = string.substring(startIndex,endIndex);
                    System.out.println(string);
                    break;

            }
            commands = scanner.nextLine();
        }
    }
}
