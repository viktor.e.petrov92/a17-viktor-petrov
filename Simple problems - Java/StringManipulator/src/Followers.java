import java.util.*;
import java.util.stream.Collectors;

public class Followers {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Map<String, Integer> nameLikes = new HashMap<>();
        Map<String, Integer> nameComments = new HashMap<>();
        String input = "";
        while (!"Log out".equals(input = scanner.nextLine())) {
            String[] token = input.split(": ");
            String command = token[0];
            switch (command) {
                case "New follower":
                    String username = token[1];
                    nameLikes.putIfAbsent(username, 0);
                    nameComments.putIfAbsent(username, 0);
                    break;
                case "Like":
                    username = token[1];
                    int count = Integer.parseInt(token[2]);
                    if (!nameLikes.containsKey(username)) {
                        nameLikes.putIfAbsent(username, count);
                        nameComments.putIfAbsent(username,0);
                    } else {
                        nameLikes.put(username, nameLikes.get(username) + count);
                    }
                    break;
                case "Comment":
                    username = token[1];
                    if (!nameComments.containsKey(username)) {
                        nameComments.put(username, 1);
                        nameLikes.putIfAbsent(username, 0);
                    } else {
                        nameComments.put(username, nameComments.get(username) + 1);
                    }
                    break;
                case "Blocked":
                    username = token[1];
                    if (nameLikes.containsKey(username)) {
                        if (nameComments.containsKey(username)) {
                            nameComments.remove(username);
                            nameLikes.remove(username);
                        }
                    } else {
                        System.out.println(username + " doesn't exist.");
                    }
                    break;
            }
        }
        List<String> names = nameLikes.entrySet().stream()
                .sorted((a, b) -> {
                    int result = Integer.compare(b.getValue(), a.getValue());
                    if (result == 0) {
                        result = a.getKey().compareTo(b.getKey());
                    }
                    return result;
                }).map(entry -> entry.getKey())
                .collect(Collectors.toList());
        System.out.println(names.size() + " followers");

        for (String s :
                names) {
            System.out.printf("%s: %d%n", s, nameLikes.get(s)+nameComments.get(s));
        }
    }
}
