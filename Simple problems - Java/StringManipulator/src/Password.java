import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Password {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());
        String regex = "^(.+)\\>(?<digits>[\\d]{3})\\|(?<lower>[a-z]{3})\\|(?<upper>[A-Z]{3})\\|(?<sym>[^><]{3})\\<\\1$";
        Pattern pattern = Pattern.compile(regex);
        for (int i = 0; i < n; i++) {
            String input = scanner.nextLine();
            Matcher matcher = pattern.matcher(input);
            if (matcher.find()) {
                String digits = matcher.group("digits");
                String lower = matcher.group("lower");
                String upper = matcher.group("upper");
                String symbols = matcher.group("sym");
                String result = digits +
                        lower +
                        upper + symbols;
                System.out.println("Password: " + result);

            } else {
                System.out.println("Try another password!");
            }

        }
    }
}
