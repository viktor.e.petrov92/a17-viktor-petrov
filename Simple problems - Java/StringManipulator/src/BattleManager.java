import java.util.*;
import java.util.stream.Collectors;

public class BattleManager {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Map<String,Integer> personHP= new HashMap<>();
        Map<String,Integer> personEN= new HashMap<>();


        String input="";
        while (!"Results".equals(input=scanner.nextLine())){
            String[] tokens= input.split(":");
            String command = tokens[0];
            switch (command){
                case "Add":
                    String name = tokens[1];
                    int hp=Integer.parseInt(tokens[2]);
                    int en=Integer.parseInt(tokens[3]);

                    if (personHP.containsKey(name)){
                        personHP.put(name,personHP.get(name)+hp);
                    }else{
                        personHP.put(name,hp);
                        personEN.put(name,en);
                    }
                    break;
                case "Attack":
                    String attacker = tokens[1];
                    String defender = tokens[2];
                    int damage = Integer.parseInt(tokens[3]);
                    if (personHP.containsKey(attacker)&&personEN.containsKey(attacker)){
                        if (personHP.containsKey(defender)&&personEN.containsKey(defender)){
                            personHP.put(defender,personHP.get(defender)-damage);
                            if (personHP.get(defender)<=0){
                                disqualify(defender);
                                personHP.remove(defender);
                                personEN.remove(defender);

                            }
                            personEN.put(attacker,personEN.get(attacker)-1);
                            if (personEN.get(attacker)<=0){
                                disqualify(attacker);
                                personHP.remove(attacker);
                                personEN.remove(attacker);

                            }
                        }
                    }
                    break;
                case"Delete":
                    String temp = tokens[1];
                    if (personHP.containsKey(temp)){
                        if (personEN.containsKey(temp)){
                            personHP.remove(temp);
                            personEN.remove(temp);

                        }
                    }
                    if (temp.equals("All")){
                        personEN.clear();
                        personHP.clear();

                    }
                    break;
            }
        }
        List<String> names = personHP.entrySet().stream()
                .sorted((a,b)->{
                    int hpA= a.getValue();
                    String nameA=a.getKey();
                    int hpB= b.getValue();
                    String nameB=b.getKey();
                    if (hpA!=hpB){
                        return Integer.compare(hpB,hpA);
                    }else{
                        return nameA.compareTo(nameB);
                    }

                }).map(entry->entry.getKey())
                .collect(Collectors.toList());
        System.out.println("People count: "+ names.size());
        for (String s :
                names) {
            System.out.printf("%s - %d - %d%n",s,personHP.get(s),personEN.get(s));
        }
    }
    public static void disqualify(String id){
        System.out.println(id+ " was disqualified!");
    }

}
