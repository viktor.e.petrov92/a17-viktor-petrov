import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class MessageEncrypter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String regex = "([@*])(?<tag>[A-Z][a-z]{2,})(\\1): \\[(?<s1>[A-Za-z])\\]\\|\\[(?<s2>[A-Za-z])\\]\\|\\[(?<s3>[A-Za-z])\\]\\|$";
        Pattern pattern = Pattern.compile(regex);
        int n = Integer.parseInt(scanner.nextLine());
        for (int i = 0; i < n; i++) {
            String input = scanner.nextLine();
            Matcher matcher = pattern.matcher(input);
            if (matcher.find()) {
                String tag = matcher.group("tag");
                String s1 = matcher.group("s1");
                String s2 = matcher.group("s2");
                String s3 = matcher.group("s3");
                System.out.printf("%s: %d %d %d%n", tag, (int) s1.charAt(0), (int) s2.charAt(0), (int) s3.charAt(0));

            } else {
                System.out.println("Valid message not found!");
            }
        }

    }
}
