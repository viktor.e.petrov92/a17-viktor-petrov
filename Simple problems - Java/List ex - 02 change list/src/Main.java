import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        List<String> elements = Arrays.stream(input.split("\\s+"))
              .collect(Collectors.toList());
        String line;
        while (!"end".equals(line = scanner.nextLine())) {
            String[] strings = line.split("\\s+");
            String command = strings[0];
            switch (command) {
                case "Delete":
                    String elem = strings[1];
                    while (elements.contains(elem)) {
                        elements.remove(elem);
                    }
                    break;
                case "Insert":
                    elem = strings[1];
                    int pos = Integer.parseInt(strings[2]);
                    if (0<=pos&&pos<elements.size())
                    elements.add(pos, elem);
                    break;

            }

        }
        System.out.println(elements.toString().replaceAll("[\\]\\[,]",""));
    }
}