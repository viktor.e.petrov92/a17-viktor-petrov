import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] numbers = scanner.nextLine().split(" ");
        String[] reversedNumbers = new String[numbers.length];
        int j = 0;
        for (int i = numbers.length - 1; i >= 0; i--) {
            reversedNumbers[j] = numbers[i];
            j++;
        }
        System.out.println(String.join(", ", reversedNumbers));
    }
}
