import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());
        int bestValue = 0;
        int snow=0;
        int time=0;
        int quality=0;
        int snow2 =0;
        int time2=0;
        int quality2=0;
        while (n > 0) {
            snow = Integer.parseInt(scanner.nextLine());
            time = Integer.parseInt(scanner.nextLine());
            quality = Integer.parseInt(scanner.nextLine());
            int value = (int) Math.pow((snow / time), quality);
            if (value > bestValue) {
                bestValue = value;
                snow2=snow;
                time2=time;
                quality2=quality;

            }
            n--;
        }
        System.out.println(snow2 + " : " + time2 + " = " + bestValue + " (" + quality2 + ")");
    }
}
