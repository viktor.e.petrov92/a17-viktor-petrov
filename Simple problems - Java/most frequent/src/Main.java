import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = Integer.parseInt(scanner.nextLine());
        int[] numbers = new int[N];
        for (int i = 0; i < N; i++) {
            numbers[i] = Integer.parseInt(scanner.nextLine());
        }
        Arrays.sort(numbers);
        int max_count = 1, result = numbers[0];
        int curr_count = 1;
        for (int i = 1; i < N; i++)
        {
            if (numbers[i] == numbers[i - 1])
                curr_count++;
            else
            {
                if (curr_count > max_count)
                {
                    max_count = curr_count;
                    result = numbers[i - 1];
                }
                curr_count = 1;
            }
        }
        if (curr_count > max_count)
        {
            max_count = curr_count;
            result = numbers[N - 1];
        }
        System.out.printf(result+"(%s times)",max_count);

    }

}
