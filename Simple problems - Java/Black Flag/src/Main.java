import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int days = Integer.parseInt(scanner.nextLine());
        int dailyPlunder = Integer.parseInt(scanner.nextLine());
        double expectedPlunder = scanner.nextDouble();
        double totalPlunder = 0;
        for (int day = 1; day <= days; day++) {
            totalPlunder += dailyPlunder;
            if (day % 3 == 0) {
                totalPlunder += 0.5 * dailyPlunder;
            }
            if (day % 5 == 0) {
                totalPlunder -= totalPlunder * 0.3;
            }
        }
        if (totalPlunder >= expectedPlunder) {
            System.out.printf("Ahoy! %.2f plunder gained.", totalPlunder);
        } else {
            double percentageLeft = (totalPlunder / expectedPlunder) * 100;
            System.out.printf("Collected only %.2f%% of the plunder.", percentageLeft);
        }
    }
}
