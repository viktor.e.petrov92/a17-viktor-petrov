import java.util.Arrays;
import java.util.Scanner;

public class Problem6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] numbers = Arrays.stream(scanner.nextLine().split("\\s+"))
                .mapToInt(Integer::parseInt).toArray();
        boolean isSymmetric= false;
        for (int i = 0; i <numbers.length    ; i++) {
            if (numbers[i]==numbers[numbers.length-i-1]){
                isSymmetric=true;
            }else {
                isSymmetric=false;
            }
        }
        if (isSymmetric){
            System.out.println("Yes");
        }else {
            System.out.println("No");
        }



    }


}
