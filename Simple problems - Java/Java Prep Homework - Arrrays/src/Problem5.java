import java.util.Arrays;
import java.util.Scanner;

public class Problem5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] arr = Arrays.stream(scanner.nextLine().split("\\s+"))
                .mapToInt(Integer::parseInt).toArray();
        int max = Integer.MIN_VALUE;
        int secondToMax = Integer.MIN_VALUE;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > max) {
                secondToMax = max;
                max = arr[i];
            } else if (arr[i] > secondToMax && arr[i] != max) {
                secondToMax = arr[i];
            }else if (arr[i] > secondToMax && arr[i] == max){
                secondToMax = arr[i];
            }
        }
        System.out.println(max + " " + secondToMax);
    }
}
