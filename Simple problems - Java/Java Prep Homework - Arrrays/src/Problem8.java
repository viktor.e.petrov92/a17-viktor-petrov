import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Problem8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] arr = Arrays.stream(scanner.nextLine().split("\\s+"))
                .mapToInt(Integer::parseInt).toArray();
        List<Integer> negative = new ArrayList<>();
        List<Integer> positive = new ArrayList<>();

        for (int i = 0; i < arr.length; i++) {
            if (arr[i]<0){
                negative.add(arr[i]);
            }else{
                positive.add(arr[i]);
            }
        }
        negative.addAll(positive);
        for (Integer a : negative) {
            System.out.print(a + " ");
        }
    }
}
