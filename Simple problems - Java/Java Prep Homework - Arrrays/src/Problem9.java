import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Problem9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] array = Arrays.stream(scanner.nextLine().split("\\s+"))
                .mapToInt(Integer::parseInt).toArray();
        List<Integer> resultList = new ArrayList<>();
        List<Integer> tempList = new ArrayList<>();
        for (int i = 0; i < array.length - 1; i++) {
            int currentNum = array[i];
            int nextNum = array[i + 1];
            tempList.add(currentNum);
            while (currentNum < nextNum) {
                tempList.add(nextNum);
                currentNum = nextNum;
                i++;
                if (i == array.length - 1) {
                    continue;
                }
                nextNum = array[i + 1];
            }
            if (tempList.size() >= resultList.size()) {
                resultList.clear();
                resultList.addAll(tempList);
            }
            tempList.clear();
        }
        for (Integer x:resultList
             ) {
            System.out.print(x+ " ");
        }
    }
}

