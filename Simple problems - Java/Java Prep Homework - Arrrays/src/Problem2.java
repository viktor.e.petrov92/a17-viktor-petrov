import java.util.Arrays;
import java.util.Scanner;

public class Problem2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String first = scanner.nextLine()  ;
        String second = scanner.nextLine()  ;
        char[] arr1 = first.toCharArray();
        char[] arr2 = second.toCharArray();
        if (Arrays.equals(arr1,arr2)){
            System.out.println("Equal");
        }else if(arr1.length>=arr2.length){
            System.out.println("Second");
        }else {
            System.out.println("First");
        }
    }
}
