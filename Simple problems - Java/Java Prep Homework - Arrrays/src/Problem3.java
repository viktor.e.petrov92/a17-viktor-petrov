import java.util.Arrays;
import java.util.Scanner;

public class Problem3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] array = Arrays.stream(scanner.nextLine()
                .split("\\s")).mapToInt(Integer::parseInt).toArray();
        int count = 0;
        int maxCount=0;
        for (int i = 1; i <array.length ; i++) {
            if (array[i]==array[i - 1]) {
                count++;
            } else {
                count = 1;
            }
            if (count > maxCount) {
                maxCount = count;
            }
        }
        System.out.println(maxCount);
    }
}
