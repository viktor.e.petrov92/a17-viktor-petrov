import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());
        int[] arr1 = new int[n];
        int[] arr2 = new int[n];
        for (int i = 0; i <n ; i++) {
            arr1[i]= Integer.parseInt(scanner.nextLine());
        }
        for (int i = 0 ; i <n ; i++) {
            arr2[i]= Integer.parseInt(scanner.nextLine());
        }
        if (Arrays.equals(arr1,arr2)){
            System.out.println("Equal");
        }else{
            System.out.println("Not equal");
        }

    }
}
