import java.util.Arrays;
import java.util.Scanner;

public class Problem7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] arr = Arrays.stream(scanner.nextLine().split("\\s+"))
                .mapToInt(Integer::parseInt).toArray();
        int n = arr.length;
        boolean result=false;
        if (arr[1] > arr[0] && arr[1] > arr[2]) {
            for (int i = 1; i < n - 1; i += 2) {
                if (arr[i] > arr[i - 1] && arr[i] > arr[i + 1]) {
                    result = true;
                }
                else {
                    result = false;
                    break;
                }
            }
            if (result && n % 2 == 0) {
                if (arr[n - 1] <= arr[n - 2]) {
                    result = false;
                }
            }
        }
        else if (arr[1] < arr[0] && arr[1] < arr[2]) {
            for (int i = 1; i < n - 1; i += 2) {
                if (arr[i] < arr[i - 1] && arr[i] < arr[i + 1]) {
                    result = true;
                }
                else {
                    result = false;
                    break;
                }
            }
            if (result && n % 2 == 0) {
                if (arr[n - 1] >= arr[n - 2]) {
                    result = false;
                }
            }
        }
        System.out.println(result);
    }
}
