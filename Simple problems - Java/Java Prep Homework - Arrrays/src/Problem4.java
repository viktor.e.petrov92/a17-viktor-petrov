import java.util.Arrays;
import java.util.Scanner;

public class Problem4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] arr = Arrays.stream(scanner.nextLine().split("\\s+"))
                .mapToInt(Integer::parseInt).toArray();
        boolean isArrSorted = false;
        for (int i = 1; i < arr.length; i++) {
            isArrSorted= arr[i - 1] <= arr[i];
        }
        if (isArrSorted){
            System.out.println("Yes");
        }else {
            System.out.println("No");
        }
    }
}
