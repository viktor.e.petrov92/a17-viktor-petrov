import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());
        int[] numbers = new int[n];
        int counter = 1;
        int longest = 1;
        for (int i = 0; i < n; i++) {
            numbers[i] = Integer.parseInt(scanner.nextLine());
        }
        for (int i = 1; i < n; i++) {
            if (numbers[i - 1] < numbers[i]) {
                counter++;
            } else {
                counter = 1;
            }
            if (counter > longest) {
                longest = counter;
            }
        }
        if (counter > longest) {
            System.out.println(counter);
        } else {
            System.out.println(longest);
        }
    }
}

