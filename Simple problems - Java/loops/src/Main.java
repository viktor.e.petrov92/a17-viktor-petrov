import java.text.DecimalFormat;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        DecimalFormat dc = new DecimalFormat("######.00");
        Scanner scanner = new Scanner(System.in);
        float max = Integer.MIN_VALUE;
        int N = scanner.nextInt();
        float[] numbers = new float[N];
        for ( int i = 0; i < numbers.length; i++) {
            numbers[i] = scanner.nextFloat();
            if (numbers[i]>max){
                max = numbers[i];
            }
        }
        System.out.println((int)max);

    }
}
