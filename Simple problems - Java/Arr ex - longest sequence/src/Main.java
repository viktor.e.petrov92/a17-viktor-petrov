import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] array = scanner.nextLine().split(" ");
        ArrayList<String> list = new ArrayList<>();
        int count = 0;
        int maxCount = 0;
        for (int i = 1; i < array.length; i++) {
            if (array[i].equals(array[i - 1])) {
                count++;
            } else {
                count = 1;
            }
            if (count > maxCount) {
                maxCount = count;
                list.clear();
                list.add(array[i]);
            }

        }
        for (int i = 0; i < maxCount; i++) {
            System.out.print(list.get(0) + " ");
        }
    }
}