import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());
        List<String> guests = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            String[] line = scanner.nextLine().split("\\s+");
            String name = line[0];
            String isGoing = line[2];
            if (isGoing.equals("going!")) {
                if (!(guests.contains(name))) {
                    guests.add(name);
                } else {
                    System.out.println(name + " is already in the list!");
                }
            }
            if (isGoing.equals("not")) {
                if (guests.contains(name)) {
                    guests.remove(name);
                } else {
                    System.out.println(name + " is not in the list!");
                }
            }
        }
        System.out.println(String.join("\n",guests));
    }
}
