import java.util.*;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] initialLoot = scanner.nextLine().split("\\|");
        ArrayList<String> loot = new ArrayList<>(Arrays.asList(initialLoot));
        ArrayList<String> stolen = new ArrayList<>();
        String input = scanner.nextLine();
        while (!input.equals("Yohoho!")) {
            String[] tokens = input.split("\\s+");
            String command = tokens[0];
            switch (command) {
                case "Loot":
                    for (int i = 1; i < tokens.length; i++) {
                        if (!loot.contains(tokens[i])) {
                            loot.add(0, tokens[i]);
                        }
                    }
                    break;
                case "Drop":
                    int index = Integer.parseInt(tokens[1]);
                    if (index >= 0 && index < loot.size()) {
                        String item = loot.remove(index);
                        loot.add(item);
                    }
                    break;
                case "Steal":
                    int count = Integer.parseInt(tokens[1]);
                    while (stolen.size() < count && !loot.isEmpty()) {
                        int lastIndex = loot.size() - 1;
                        stolen.add(loot.remove(lastIndex));
                    }
                    Collections.reverse(stolen);
                    System.out.println(String.join(", ", stolen));
                    break;

            }
            input = scanner.nextLine();
        }
        if (loot.isEmpty()) {
            System.out.println("Failed treasure hunt.");
        } else {
            double sum = 0;
            for (String str : loot
            ) {
                sum += str.length();
            }
            sum /= loot.size();
            System.out.printf("Average treasure gain: %.2f pirate credits.", sum);
        }
    }
}
