import java.text.DecimalFormat;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        DecimalFormat df = new DecimalFormat("###.00");
        int gamesLost = Integer.parseInt(scanner.nextLine());
        double headsetPrice = Double.parseDouble(scanner.nextLine());
        double mousePrice = Double.parseDouble(scanner.nextLine());
        double keyboardPrice = Double.parseDouble(scanner.nextLine());
        double displayPrice = Double.parseDouble(scanner.nextLine());
        int headsetCount = 0;
        int mouseCount = 0;
        int keyboardCount = 0;
        int displayCount = 0;
        for (int i = 1; i <= gamesLost; i++) {
            if (i % 2 == 0) {
                headsetCount += 1;
            }
            if (i % 3 == 0) {
                mouseCount += 1;
            }
            if (i % 2 == 0 && i % 3 == 0) {
                keyboardCount += 1;
            }
        }
        for (int i = 1; i <= keyboardCount; i++) {
            if (i % 2 == 0) {
                displayCount += 1;
            }
        }
        double totalHeadsetPrice = headsetPrice * headsetCount;
        double totalMousePrice = mousePrice * mouseCount;
        double totalKeyboardPrice = keyboardPrice * keyboardCount;
        double totalDisplayPrice = displayPrice * displayCount;
        double sum = totalDisplayPrice + totalHeadsetPrice + totalKeyboardPrice + totalMousePrice;
        System.out.println("Rage expenses: " + df.format(sum) + " lv.");
    }
}

