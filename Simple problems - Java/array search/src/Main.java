import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] line = scanner.nextLine().split(",");
        int n = line.length;
        Set<Integer> set = new HashSet<>();
        for (String s : line) {
            set.add(Integer.parseInt(s));
        }
        boolean found = false;
        for (int i = 1; i <= n; i++) {
            if (!set.contains(i)) {
                if (found) {
                    System.out.print(",");
                }
                System.out.print(i);
                found = true;
            }
        }
    }
}
