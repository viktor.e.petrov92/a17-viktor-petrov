import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());
        int[] numbers = new int[n];
        for (int i = 0; i < n; i++) {
            numbers[i] = Integer.parseInt(scanner.nextLine());
        }
        System.out.println(maximumSubsequenceSum(numbers));
    }

    private static int maximumSubsequenceSum(int[] arr) {
        int maxSoFar = 0;
        int maxEndingHere = 0;
        for (int item : arr) {
            maxEndingHere = Math.max(maxEndingHere + item, 0);
            maxSoFar = Math.max(maxSoFar, maxEndingHere);
        }
        return maxSoFar;
    }
}
