import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] input = scanner.nextLine().split(",");
        Integer[] integers = new Integer[input.length];
        List<Integer> posNum = new ArrayList<>();
        List<Integer> negNum = new ArrayList<>();
        List<Integer> zeros = new ArrayList<>();
        for (int i = 0; i < input.length; i++) {
            integers[i] = Integer.parseInt(input[i]);
            if (integers[i] > 0) {
                posNum.add(integers[i]);
            } else if (integers[i] < 0) {
                negNum.add(integers[i]);
            } else {
                zeros.add(integers[i]);
            }
        }
        negNum.addAll(zeros);
        negNum.addAll(posNum);
        System.out.print(Arrays.toString(new List[]{negNum})
                .replace("[", "")
                .replace("[", "")
                .replace("]]", "")
                .replace("[[", "")
                .replace(" ", ""));

    }
}
