import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] input = scanner.nextLine().split(",");
        String[] binary = new String[input.length];
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < input.length; i++) {
            binary[i] = Integer.toBinaryString(Integer.parseInt(input[i]));
            while (binary[i].length() < 8) {
                binary[i] = "0" + binary[i];
            }
            if (i % 2 == 0) {
                for (int j = 1; j < 8; j += 2) {
                    result.append(binary[i].charAt(j));
                }
            } else {
                for (int j = 0; j < 8; j += 2) {
                    result.append(binary[i].charAt(j));
                }
            }
        }
        System.out.println(result);
    }
}
