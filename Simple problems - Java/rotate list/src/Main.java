import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();
        String[] arr = line.split(" ");
        int N = scanner.nextInt();
        Collections.rotate(Arrays.asList(arr), -N);
        System.out.println(Arrays.toString(arr)
                .replace("[", "")
                .replace("]", "")
                .replaceAll(",", ""));
    }
}
