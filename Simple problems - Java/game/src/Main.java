import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String number = scanner.nextLine();
        int a = number.charAt(0) - '0';
        int b = number.charAt(1) - '0';
        int c = number.charAt(2) - '0';
        int result=-1;
        result=Math.max(result, a+b+c);
        result=Math.max(result, a+b*c);
        result=Math.max(result, a*b+c);
        result=Math.max(result, a*b*c);
        System.out.println(result);
    }
}