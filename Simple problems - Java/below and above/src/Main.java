import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        DecimalFormat df = new DecimalFormat("##0.00");
        String[] input = scanner.nextLine().split(",");
        Integer[] integers = new Integer[input.length];
        List<Integer> above = new ArrayList<>();
        List<Integer> below = new ArrayList<>();
        int total = 0;

        for (int i = 0; i < input.length; i++) {
            integers[i] = Integer.parseInt(input[i]);
            total = total + integers[i];
        }
        double average = (double) total / integers.length;
        for (int j = 0; j < integers.length; j++) {
            if (integers[j] > average) {
                above.add(integers[j]);
            } else if(integers[j] < average) {
                below.add(integers[j]);
            }
        }
        System.out.println("avg: " + df.format(average));
        System.out.println("below: " + Arrays.toString(new List[]{below})
                .replace("[", "")
                .replace("[", "")
                .replace("]]", "")
                .replace("[[", "")
                .replace(" ", ""));
        System.out.println("above: " + Arrays.toString(new List[]{above})
                .replace("[", "")
                .replace("[", "")
                .replace("]]", "")
                .replace("[[", "")
                .replace(" ", ""));
    }
}

