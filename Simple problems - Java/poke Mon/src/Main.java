import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int pokePower = Integer.parseInt(scanner.nextLine());
        int distance = Integer.parseInt(scanner.nextLine());
        int exhaustionFactor = Integer.parseInt(scanner.nextLine());
        int pokePowerCopy = pokePower;
        int targetCount = 0;
        while (pokePower >= distance) {
            pokePower -= distance;
            targetCount++;
            if (pokePowerCopy / 2 == pokePower) {
                if (exhaustionFactor > 0) {
                    pokePower = pokePower / exhaustionFactor;
                }
            }

        }
        System.out.println(pokePower);
        System.out.println(targetCount);
    }
}
