import java.text.DecimalFormat;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        DecimalFormat dc = new DecimalFormat("###.###");
        double a = scanner.nextDouble();
        double b = scanner.nextDouble();
        double c = scanner.nextDouble();
        if (a >= b && a >= c) {
            System.out.println(dc.format(a));
        }else
        if (b >= a && b >= c) {
            System.out.println(dc.format(b));
        }else
        if (c >= a && c >= b) {
            System.out.println(dc.format(c));
        }
    }
}
