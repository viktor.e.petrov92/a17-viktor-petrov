import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());
        String[] first = new String[n];
        String[] second = new String[n];
        for (int i = 1; i <= n; i++) {
            String[] input = scanner.nextLine().split(" ");
            if (i % 2 != 0) {
                first[i - 1] = input[0];
                second[i - 1] = input[1];
            } else {
                first[i - 1] = input[1];
                second[i - 1] = input[0];
            }
        }
        System.out.println(Arrays.toString(first)
                .replace("[", "")
                .replace("]", "")
                .replace(",", ""));
        System.out.println(Arrays.toString(second)
                .replace("[", "")
                .replace("]", "")
                .replace(",", ""));
    }
}
