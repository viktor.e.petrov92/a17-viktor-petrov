
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        List<Integer> wagons = Arrays.stream(input.split(" "))
                .map(Integer::parseInt).collect(Collectors.toList());
        int maxCapacity = Integer.parseInt(scanner.nextLine());
        String line;
        while (!"end".equals(line = scanner.nextLine())) {
            String[] tokens = line.split(" ");
            if (tokens[0].equals("Add")) {
                int passengers = Integer.parseInt(tokens[1]);
                wagons.add(passengers);
            } else {
                int pass = Integer.parseInt(tokens[0]);
                for (int i = 0; i < wagons.size(); i++) {
                    int temp = wagons.get(i) + pass;
                    if (temp <= maxCapacity) {
                        wagons.set(i, temp);
                        break;
                    }
                }
            }

        }
        for (Integer wagon : wagons) {
            System.out.print(wagon + " ");
        }
    }
}
