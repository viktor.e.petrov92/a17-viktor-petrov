import java.text.DecimalFormat;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        DecimalFormat df = new DecimalFormat("###########.00");
        Scanner scanner = new Scanner(System.in);
        long a = Long.parseLong(scanner.nextLine());
        long b = Long.parseLong(scanner.nextLine());
        System.out.printf("%.2f", (double) Factorial(a) / Factorial(b));

    }
    private static double Factorial(double n) {
        if (n == 0)
            return 1;
        else
            return (n * Factorial(n - 1));
    }
}
