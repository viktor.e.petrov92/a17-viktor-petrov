import java.text.DecimalFormat;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        DecimalFormat df = new DecimalFormat("###.00");
        double money = Double.parseDouble(scanner.nextLine());
        int students = Integer.parseInt(scanner.nextLine());
        double lightsabersPrice = scanner.nextDouble();
        double robePrice = scanner.nextDouble();
        double beltPrice = scanner.nextDouble();
        double lightSum = lightsabersPrice * (students + Math.ceil(students * 0.1));
        double robeSum = robePrice * students;
        int counter = 0;
        double beltSum;
        for (int i = 1; i <= students; i++) {
            if (i % 6 == 0) {
                counter++;
            }
        }
        beltSum = beltPrice * (students - counter);
        double totalSum = lightSum + robeSum + beltSum;
        double moneyNeed = totalSum - money;
        if (money >= totalSum) {
            System.out.println("The money is enough - it would cost " + df.format(totalSum) + "lv.");
        } else {
            System.out.println("Ivan Cho will need " + df.format(moneyNeed) + "lv more.");
        }

    }
}
