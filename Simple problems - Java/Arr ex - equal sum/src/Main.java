import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] number = Arrays.stream(scanner.nextLine().split("\\s"))
                .mapToInt(Integer::parseInt).toArray();
        int sumLeft = 0;
        int sumRight = 0;

        for (int i = 0; i <= number.length - 1; i++) {
            if (number.length == 1) {
                System.out.println(0);
                return;
            }
            sumLeft = 0;
            for (int iLeft = i; iLeft > 0; iLeft--) {
                int nextPosition = iLeft - 1;
                if (iLeft > 0) {
                    sumLeft += number[nextPosition];
                }
            }

            sumRight = 0;
            for (int iRight = i; iRight < number.length; iRight++) {
                int nextPosition = (iRight + 1);
                if (iRight < number.length - 1) {
                    sumRight += number[nextPosition];
                }
            }

            if (sumLeft == sumRight) {
                System.out.println(i);
                return;
            }

        }System.out.println("no");
    }
}
