import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int smallBars = Integer.parseInt(scanner.nextLine());
        int bigBars = Integer.parseInt(scanner.nextLine());
        int kilosToCarry = Integer.parseInt(scanner.nextLine());
        int bigBarsKg = bigBars * 5;
        int smallBarsKg = 1;
        int sum = 0;
        int smallBarsCount = 0;
        while (kilosToCarry!=0) {
            sum += bigBarsKg;
            while (sum <= kilosToCarry) {
                sum += smallBarsKg;
                smallBarsCount++;
                if (smallBarsCount > smallBars) {
                    smallBarsCount = smallBars;
                }
            }
            kilosToCarry--;
        }

        if (sum < kilosToCarry) {
            System.out.println("-1");
        } else {
            System.out.println(smallBarsCount);
        }


    }
}
