import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        List<Integer> numbers = Arrays.stream(input.split("\\s+"))
                .map(Integer::parseInt).collect(Collectors.toList());
        String[] bomb = scanner.nextLine().split("\\s+");
        int bombNumber = Integer.parseInt(bomb[0]);
        int power = Integer.parseInt(bomb[1]);
        while (numbers.contains(bombNumber)) {
            int bombPos = numbers.indexOf(bombNumber);
            int leftBound = Math.max(0, bombPos - power);
            int rightBound = Math.min(numbers.size() - 1, bombPos + power);
            for (int i = rightBound; i >=leftBound ; i--) {
                numbers.remove(i);
            }
        }
        int sum =0;
        for (int elem :numbers
             ) {
            sum+=elem;
        }
        System.out.println(sum);
    }
}
